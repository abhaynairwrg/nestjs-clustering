import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppClusterService } from './app-cluster/app-cluster.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, AppClusterService],
})
export class AppModule {}
